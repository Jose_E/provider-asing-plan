<?php
/*
Plugin Name: HEC Provider Asing plans
Description: assign by groups of pages the plans of the providers
Author: Gnovus
Version: 1.2.0
Text Domain:       provider_asing_plan
 */
/*class provider_asing_plan {
public function __construct()
{ */
    use App\Controllers\Enrollment\ConnectAPI;
    add_action('wp_head','add_ajax_library');
    add_action('wp_ajax_hec_addDatarow', 'hec_addDatarow');
    add_action('wp_ajax_newadd', 'newadd');
    add_action('wp_ajax_delete', 'delete_register');
    
    define('RJC_DIR',plugin_dir_path(__FILE__));
    add_action( 'admin_menu', 'rjc_menu_administrador' );
    register_activation_hook( __FILE__, 'create_db');
// font-awesome.min.css
        
    
    function rjc_menu_administrador() {
    add_menu_page(
        'Page list group plans',
        'Page asing plans',
        'manage_options',
        RJC_DIR.'/view/view_list_register.php');
        add_submenu_page(RJC_DIR . '/view/view_list_register.php',
        'New group',
        'New group',
        'manage_options',
        RJC_DIR . '/view/add_new_group.php');
        // Enqueue the main Stylesheet.

        wp_enqueue_style( 'fontawesome_css', plugins_url('css/font-awesome.min.css', __FILE__));
        wp_enqueue_style('hec-plugin-css-multiselect', plugins_url( 'assets/css/class.css', __FILE__));
        wp_register_style('select2-css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
        wp_enqueue_style('select2-css');
        

        // Enqueue the main Scripts.
        wp_enqueue_script( 'index-select-js', plugins_url( 'assets/js/index-select.js', __FILE__));
        wp_register_script( 'select2-min', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', null, null, true );
        wp_enqueue_script ( 'select2-min' );

        //$ajaxUrl_selec = admin_url('admin-ajax.php');
        //$html = "<script type='text/javascript'> var ajaxurl_selec = '$ajaxUrl_selec'</script>";
        //echo $html;
    }

     /**
     * Adds the WordPress Ajax Library to the frontend.
     */
     function add_ajax_library()
    {
        $ajaxurl = admin_url('admin-ajax.php');
        $html = "<script type='text/javascript'> var ajaxurl = '$ajaxurl'</script>";
        echo $html;
    }
    /**
     * Create Table for plugin
	 */
    function create_db() {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'group_plans';
        //die(print_r('ok ok '));
        $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            name_group varchar(255) NOT NULL,
            pageposid text NOT NULL,
            rate_provider text NOT NULL,
            UNIQUE KEY id (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
  /*  
    if(!function_exists('mi_providerasing_function')){
        function mi_providerasing_function(){
            // Contenido de la función
        }
    }
*/
//}

function hec_addDatarow()
{//cont

    $response = ['messages' => [], 'status' => 'success', 'response'=>''];
    $selec.=' <select  name="post_provider[]"  id="provider" autocomplete="off">';
      $connectAPI = new ConnectAPI();
      $_ParamasAllproviders['provider'] ='All';
      $AllProviders = $connectAPI->conectREST($_ParamasAllproviders);
      $field['choices']['0'] = 'Select Provider';
      foreach ($AllProviders['body'] as $OneProvider) 
      {
          if($OneProvider['provider'] != ""){
              if($OneProvider['provider'] === 'Tri-Eagle')
              {
                  $OneProvider['provider']='TriEagle';
              }
             $provider_energy =$OneProvider['provider'];
              if( in_array($OneProvider['provider'], ['Champion','Reliant','TriEagle','Entrust','4Change','Cirro','Lone Star','Gexa','Chariot','Just','Tara','Amigo']) )
              {
                $OneProvider['provider']=$OneProvider['provider'].' Energy'; 
              }
              $selec.='<option value="'.$provider_energy.'|##|'.$OneProvider['provider_phone'].'">'.$OneProvider['provider'].'</option>';
          }
      }
    
      $selec.='</select>';
    $row='';
    $row.='<tr>';
    $row.='<td style="display: none"><input type="hidden" value="'.$_POST['cont'].'" name="ids[]"></td>';
    $row.='<td><input type="text" name="post_title_table[]"  value="" id="Title" spellcheck="true" autocomplete="off" class="input"></td>';
    $row.='<td>';
    $row.= $selec;
    $row.='</td>';
    $row.='<td><input type="text" name="post_zipcode[]"  value="" id="Zipcode" spellcheck="true" autocomplete="off"  class="input"></td>';
    $row.='<td>';
    $row.='<textarea name="post_button[]" id="button" rows="5" cols="15"></textarea>'; 
    $row.='</td>';
    $row.='<td><input type="text" name="post_tel[]"  value="" id="Tel" spellcheck="true" autocomplete="off" class="input"></td>';
    $row.='<td>';
    $row.='<textarea name="post_text_complement[]" id="post_text_complement" rows="5" cols="15" ></textarea>';
    $row.='</td>';
    $row.='<td ><a href="javascript:void(0)" class="ph-button ph-btn-red deleteRow" id="deleteRow" onclick="if(confirm(\'Are you sure to remove?\')){jQuery(this).closest(\'tr\').remove();}"><i class="fa fa-fw fa-trash-o"></i>-</a></td>';
    $row.='</tr>';
    $response['response']=$row;
    wp_send_json($response);
}
function delete_register()
{
    $response = ['messages' => '', 'status' => 'false', 'response'=>''];
    if(isset($_POST['id']) && $_POST['id'] !="")
    { 
    global $wpdb;
           
    $id=$_POST['id'];
    $sql="DELETE FROM wpqd_group_plans WHERE id=".$id;
    $wpdb->query($sql);
    $sql2="select * FROM wpqd_group_plans WHERE id=".$id;
    $register = $wpdb->get_results($sql2);
    if(count($register) <= 0)
    {
        $response = ['messages' => 'Group deleted successfully', 'status' => 'success', 'response'=>''];
    }else
    {
        $response = ['messages' => 'Could not delete record, please try again later', 'status' => 'false', 'response'=>''];
    }

    }
    wp_send_json($response);
}
function newadd()
{
    $response = ['messages' => '', 'status' => 'false', 'response'=>''];
    $pages="";
    global $wpdb;
    $page_array=array();
    if(isset($_POST['states']) && count($_POST['states']) > 0)
    {   $x=0;
        foreach($_POST['states'] as $val)
        {
            $pages.='{"PageId":'.$val.'},';
            $page_array[$x] = ["PageId"=>$val];
            $x++;
        }
    }
    $table="";
    $ids_pages=implode(", ", $_POST['states']);
    $all=$_POST;
    $_id = $_POST['ids'];
    $tt = $_POST['post_title_table'];
    $post_provider = $_POST['post_provider'];
    $post_button = $_POST['post_button'];
    $tel=$_POST['post_tel'];
    $complement = $_POST['post_text_complement'];
    $i=0;
    $arrya_table=array();
    foreach($_POST['post_zipcode'] as $key => $val)
    {
        $prov=explode("|##|",$post_provider[$key]);
        $table.='{ "id": "'.$_id[$key].'",
            "Title": "'.$tt[$key].'",
            "provider": "'.$prov[0].'",
            "Zipcode": "'.$val.'",
            "Button": "'.str_replace(['"',"'"],["|##|","|**|"],$post_button[$key]).'",
            "Tel": "'.$tel[$key].'",
            "Text_complement": "'.str_replace(['"',"'"],["|##|","|**|"],$complement[$key]).'"
        },';
        $arrya_table[$i]=["id"=>$_id[$key], 
                        "Title"=>$tt[$key], 
                        "provider"=>$prov[0], 
                        "Zipcode" => $val, 
                        "Button"=> str_replace(['"',"'"],["|##|","|**|"],$post_button[$key]),
                        "Tel"=> $tel[$key],
                        "Text_complement"=>str_replace(['"',"'"],["|##|","|**|"],$complement[$key])];
        $i++;
    }
    $jason_array=["Name"=>$_POST['post_title'],"show_pages" =>$page_array,"plan_provider" => $arrya_table];
    $json='{
        "Name": "'.$_POST['post_title'].'",
        "show_pages": ['.substr($pages, 0, -1).'],
        "plan_provider": ['.substr($table, 0, -1).']
    }';
     //   die(print_r(); ,JSON_UNESCAPED_SLASHES
      $rd = addslashes(json_encode($jason_array));//json_decode($json);
//	$rd= $jason_array;	     
 // die(print_r($rd));
       if($_POST['idregister'] != "")
       {

        $query_insert="update ".$wpdb->prefix."group_plans set rate_provider='".json_encode($rd)."', name_group ='".$_POST['post_title']."', pageposid='".$ids_pages."' where id= ".$_POST['idregister'];
       }
       else
       {
        $query_insert="INSERT INTO ".$wpdb->prefix."group_plans (name_group, pageposid, rate_provider) VALUES ('".$_POST['post_title']."', '".$ids_pages."', '".json_encode($rd)."')";
       }
    
  //  die(print_r($query_insert));
    $wpdb->query($query_insert);

    $provider_plans = $wpdb->get_results("SELECT id  FROM ".$wpdb->prefix."group_plans where pageposid = '".$ids_pages."' and name_group= '".$_POST['post_title']."'");
    if(count($provider_plans) > 0)
    {
        foreach ($provider_plans as $plans) {
            $response = ['messages' => 'Registration entered successfully', 'status' => 'success', 'response'=>$plans->id];
        }
    }
    else
    {
        $response = ['messages' => 'Failed to save record, please try again later', 'status' => 'false', 'response'=>''];
    }
    wp_send_json($response);
}
