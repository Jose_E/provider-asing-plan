<?php 

use App\Controllers\Enrollment\ConnectAPI;
global $wpdb;
$pages = $wpdb->get_results("SELECT ID as id,post_title FROM wpqd_posts wp WHERE  wp.post_type in ('new_posts','new_pages') ");
$cont=1;
$id="";
$name="";
$pages_select="";
$tablesProb="";
$countdat=0;
if(isset($_GET['registerid']))
{
   $provider_plans = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."group_plans where id =".$_GET['registerid']);
   if(count($provider_plans) > 0)
   {
      foreach($provider_plans as $val)
      {
         $id =$val->id;
         $name= $val->name_group;
         $pages_select= ($val->pageposid !="") ? explode(',',$val->pageposid):"";
         $tablesProb =json_decode($val->rate_provider,true);
      // die(print_r($val->rate_provider));
      }
   }
}
//
?>
<div class="wrap">
<div class="navigation-tabs">
   <h2><?php _e('Add new group','rjc-delete-roles') ?></h2>
   <form id="form" method="post" onSubmit="return false;">
<table class="center">
<tr>
   <td>
   <div id="titlewrap">
		<h2 class="" id="title-prompt-text" for="title">Title</h2>
	<input type="text" name="post_title" size="30" value="<?php echo $name;?>" id="title" spellcheck="true" autocomplete="off" aria-required="true">
</div>
</td>
<td>
<div id="titlewrap">
		<h2 class="" id="title-prompt-text" for="title">Select pages</h2>

        <select id="required" class="js-example-basic-multiple"  name="states[]" multiple="multiple" aria-required="true">
          <?php 
            if(count($pages) > 0)
            {
                foreach ($pages as $page) {
                   if($pages_select =="")
                   {
                     echo   '<option value="'.$page->id.'" >'.$page->post_title.'</option>';
                   }else
                   {
                      if(in_array($page->id, $pages_select))
                      {
                        echo   '<option value="'.$page->id.'" selected>'.$page->post_title.'</option>';
                      }else
                      {
                        echo   '<option value="'.$page->id.'" >'.$page->post_title.'</option>';
                      }
                   }

                }
            }
          ?>
        </select>
      
</div>
</td>
</tr>   
</table>

<br/>
   <table class="wp-list-table widefat fixed striped media" style="width: 100%;">
      <thead>
         <tr>
         <th  style="display: none"></th>
         <th>Title</th>
         <th>provider</th>
         <th>Zipcode</th>
         <th>Button</th>
         <th>Tel</th> 
         <th>Text complement</th>
         <th style="width: 4%;"><a href="javascript:void(0)" class="ph-button ph-btn-green addRow" id="addmore"><i class="fa fa-fw fa-plus-circle"></i>+</a></th>        
      </tr>
   </thead>
   <tbody id="tb">
      <?php if($tablesProb == ""){?>
      <tr>
         <td style="display: none"><input type="hidden" value="<?php echo $cont;?>" name="ids[]"></td>
         <td><input type="text" name="post_title_table[]"  value="" id="Title" spellcheck="true" autocomplete="off" class='input'></td>
         <td>
            <select  name="post_provider[]"  id="provider" autocomplete="off">
         <?php 
           $connectAPI = new ConnectAPI();
           $_ParamasAllproviders['provider'] ='All';
           $AllProviders = $connectAPI->conectREST($_ParamasAllproviders);
           $field['choices']['0'] = 'Select Provider';
           foreach ($AllProviders['body'] as $OneProvider) 
           {
               if($OneProvider['provider'] != ""){
                   if($OneProvider['provider'] === 'Tri-Eagle')
                   {
                       $OneProvider['provider']='TriEagle';
                   }
                  $provider_energy =$OneProvider['provider'];
                   if( in_array($OneProvider['provider'], ['Champion','Reliant','TriEagle','Entrust','4Change','Cirro','Lone Star','Gexa','Chariot','Just','Tara','Amigo']) )
                   {
                     $OneProvider['provider']=$OneProvider['provider'].' Energy'; 
                   }
                   echo '<option value="'.$provider_energy.'|##|'.$OneProvider['provider_phone'].'">'.$OneProvider['provider'].'</option>';
               //$field['choices'][$OneProvider['provider'].'|##|'.$OneProvider['provider_phone']] = $OneProvider['provider'];
               }
           }
         ?>
         </select>
         </td>
         <td><input type="text" name="post_zipcode[]"  value="" id="Zipcode" spellcheck="true" autocomplete="off"  class='input' aria-required="true"></td>
         <td>
         <textarea name="post_button[]" id="button" rows="5" cols="15"></textarea>   <!--
         <input type="text" name="post_Button[]"  value=""  spellcheck="true" autocomplete="off" class='input'>-->
            </td>
         <td><input type="text" name="post_tel[]"  value="" id="Tel" spellcheck="true" autocomplete="off" class='input'></td>
         <td>
         <textarea name="post_text_complement[]" id="post_Text_complement" rows="5" cols="15" ></textarea>
         </td>
         <td ><a href="javascript:void(0)" class="ph-button ph-btn-red deleteRow" id="deleteRow" onclick="if(confirm('Are you sure to remove?')){jQuery(this).closest('tr').remove();}"><i class="fa fa-fw fa-trash-o"></i>-</a></td>
      </tr>
      <?php }
      else{
         $json_val= json_decode($tablesProb,true);
       // die(print_r(json_decode($json_val))); 
	$connectAPI = new ConnectAPI();
         $_ParamasAllproviders['provider'] ='All';
         $AllProviders = $connectAPI->conectREST($_ParamasAllproviders);
         if(count($json_val['plan_provider']) > 0)
         {
            foreach($json_val['plan_provider'] as $keey => $val)
            {
               $cont=$val['id'];
?>
 <tr>
         <td style="display: none"><input type="hidden" value="<?php echo $cont;?>" name="ids[]"></td>
         <td><input type="text" name="post_title_table[]"  value="<?php echo $val['Title']; ?>" id="Title" spellcheck="true" autocomplete="off" class='input'></td>
         <td>
            <select  name="post_provider[]"  id="provider" autocomplete="off">
         <?php 
           $field['choices']['0'] = 'Select Provider';
           foreach ($AllProviders['body'] as $OneProvider) 
           {
               if($OneProvider['provider'] != ""){
                   if($OneProvider['provider'] === 'Tri-Eagle')
                   {
                       $OneProvider['provider']='TriEagle';
                   }
                  $provider_energy =$OneProvider['provider'];
                   if( in_array($OneProvider['provider'], ['Champion','Reliant','TriEagle','Entrust','4Change','Cirro','Lone Star','Gexa','Chariot','Just','Tara','Amigo']) )
                   {
                     $OneProvider['provider']=$OneProvider['provider'].' Energy'; 
                   }
                   $select ='';
                   if($val['provider'] === $provider_energy )
                   {
                     $select ='selected';
                   }
                   echo '<option value="'.$provider_energy.'|##|'.$OneProvider['provider_phone'].'" '.$select.'>'.$OneProvider['provider'].'</option>';
               //$field['choices'][$OneProvider['provider'].'|##|'.$OneProvider['provider_phone']] = $OneProvider['provider'];
               }
           }
         ?>
         </select>
         </td>
         <td><input type="text" name="post_zipcode[]"  value="<?php echo $val['Zipcode']; ?>" id="Zipcode" spellcheck="true" autocomplete="off"  class='input' aria-required="true"></td>
         <td>
         <textarea name="post_button[]" id="button" rows="5" cols="15"><?php echo str_replace(["\|##|","|**|"],['"',"'"],$val['Button']); ?></textarea>   <!--
         <input type="text" name="post_Button[]"  value=""  spellcheck="true" autocomplete="off" class='input'>-->
            </td>
         <td><input type="text" name="post_tel[]"  value="<?php echo $val['Tel']; ?>" id="Tel" spellcheck="true" autocomplete="off" class='input'></td>
         <td>
         <textarea name="post_text_complement[]" id="post_Text_complement" rows="5" cols="15" ><?php echo str_replace(["\|##|","|**|"],['"',"'"],$val['Text_complement']); ?></textarea>
         </td>
         <td ><a href="javascript:void(0)" class="ph-button ph-btn-red deleteRow" id="deleteRow" onclick="if(confirm('Are you sure to remove?')){jQuery(this).closest('tr').remove();}"><i class="fa fa-fw fa-trash-o"></i>-</a></td>
      </tr>
<?php
$countdat++;
            }
         }
      }
      ?>
   </tbody>
  </table>
  
  <div class="grid-x grid-margin-x grid-padding-x">
      <div class="cell medium-2">
         <p class="submit">
            <input type="hidden" name="idregister" id="idregister" value="<?php echo  $id; ?>">
            <input type="hidden" name="countdate" id="countdate" value="<?php echo  $countdat; ?>"> 
            <button type="submit" class="button button-primary" name="hec_save_general"><i class="fa fa-fw fa-save"></i> Save</button>
            <input type="hidden" name="action" id="action" value="newadd">
         </p>
      </div>
   </div>
   </form>
</div>
</div>
